package com.eod.rds.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eod.rds.dao.ReportRepository;
import com.eod.rds.dto.ReportResponse;
import com.eod.rds.model.Report;

@Service
public class ReportServiceImpl implements ReportService {
	@Autowired
	ReportRepository repository;

	@Override
	public ReportResponse getReports() {
		return new ReportResponse(repository.findAll());
	}

	@Override
	public Report getReport(Long id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	public Report createReport(Report report) {
		return repository.save(report);
	}

}
