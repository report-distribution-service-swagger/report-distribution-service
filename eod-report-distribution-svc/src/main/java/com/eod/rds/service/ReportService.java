package com.eod.rds.service;

import com.eod.rds.dto.ReportResponse;
import com.eod.rds.model.Report;

public interface ReportService {
	public ReportResponse getReports();

	public Report getReport(Long id);

	public Report createReport(Report report);
}
