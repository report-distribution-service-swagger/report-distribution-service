package com.eod.rds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableJpaRepositories(basePackages = { "com.eod.rds.dao" })
@EntityScan(basePackages = { "com.eod.rds.model" })

public class ReportsDistributionApplication {
	public static void main(String[] args) {
		SpringApplication.run(ReportsDistributionApplication.class, args);
	}
}
