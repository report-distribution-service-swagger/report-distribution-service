package com.eod.rds.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eod.rds.dto.ReportResponse;
import com.eod.rds.model.Report;
import com.eod.rds.service.ReportService;

@RestController
@RequestMapping(produces = "application/json", path = "/api/v1/reports")
public class ReportsController {
	@Autowired
	ReportService service;

	@RequestMapping(method = RequestMethod.GET,path = "get-list-of-reports" )
	public ResponseEntity<ReportResponse> getReports() {
		ReportResponse reports = this.service.getReports();
		return ResponseEntity.ok(reports);
	}

	@RequestMapping(method = RequestMethod.GET,path = "get-report/{id}")
	public ResponseEntity<Report> getReportDetailsById(@RequestParam(name = "id") Long id) {
		Report report=this.service.getReport(id);
		return ResponseEntity.ok(report);
	}
	
	@PostMapping("/create-report")
	public ResponseEntity<Report> createReport(@RequestBody Report report) {
		try {
			Report createdReport = this.service.createReport(report);
			return new ResponseEntity<Report>(createdReport, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
