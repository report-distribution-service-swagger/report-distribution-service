package com.eod.rds.dto;

import java.util.List;
import com.eod.rds.model.Report;

public class ReportResponse {
	List<Report> reports;

	public ReportResponse(List<Report> reports) {
		super();
		this.reports = reports;
	}

	public List<Report> getReports() {
		return reports;
	}

	public void setReports(List<Report> reports) {
		this.reports = reports;
	}

}
